#!/bin/sh
clear
cd ..

if [ ! -e build/release/octopus-osx.app ]; then
    echo "octopus-osx.app shell is missing."
    echo "Download from https://github.com/rogerwang/node-webkit, save it to 'build/release/octopus-osx.app'"
    exit 1
fi

echo "Zipping..."
zip -r app.nw app index.html package.json -x *.DS_Store* > /dev/null
echo "Installing to bundle..."
mv app.nw build/release/octopus-osx.app/Contents/Resources
echo "Build complete, launching app!"
open build/release/octopus-osx.app
