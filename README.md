octopus
=======
The universal bootstrapper for game mods.

#### About

I'm a gamer, as I'm sure you've already surmised. I also make programs for fun (and for a living). In the various games I play, I see a lot of instances where game modifications (such as World of Warcraft addons, or Skyrim mods) are really fragmented or commercialized- sites such as Curse are fine and all, but I don't like the idea of having to view ads or have limited download speeds. That's where octopus comes in to play.

#### How it works

Well, it doesn't work, yet.

#### How it *will* work

The idea is to have something like what various other Open Source projects have- if you're familiar with them, think NPM, CocoaPods, or Homebrew.
Essentially, the idea is that the client will be a delivery mechanism for packaged mod installers. Users will be able to set their game install directory (possibly we'll be able to add in automatic game discovery). Then, they'll be able to search or browse for various downloads, which will download and install with a minimum of fuss.

#### How is this different? I already have Curse/WoWMatrix/other things...

The primary difference is that, to my knowledge, none of the solutions out there are open source. They're not by the community, or for the community- they exist to make money. What I'd rather do is get something started that we can all use and all contribute to, so that the experience is better for everyone.

#### I want to contribute...

Awesome! Fork the repo, get coding, submit the pull request. If you want to contribute in other ways, we'll be in need of testers, art, general feedback, all kinds of things. Just open issues and we'll get it sorted out.

#### I still have questions!

Open an issue.