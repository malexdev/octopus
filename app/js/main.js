/*global bootbox*/
'use strict';
var util = require('util');

// do any page setup here
$(document).ready(function() {
    loadNewestPackages();
});

/**
 * Load the specified script into the page dynamically
 * @param name {string} The script name
 */
function loadScript(name) {
    $.getScript('app/js/' + name);
}

/**
 * Kick off the process to load the newest packages on the page
 */
function loadNewestPackages() {
    // we want to showcase 4 of the newest packages
    for (var i = 0; i < 4; i++) {
        appendNewestPackageThumbnail(i);
    }
}

/**
 * Append a 'newest package' thumbnail to the page
 * @param count {integer} The count of this thumbnail
 */
function appendNewestPackageThumbnail(count) {
    // build variables we'll use in the html
    var img = './app/js/holder.js/300x200';
    var elementID = util.format('newest-packages-%s', count);
    var title = util.format('Title of item %s', count);
    var detail = util.format('Description of item %s', count);
    var downloadID = util.format('download-button-%s', count);
    
    // build the html for the thumbnail
    var html = 
        util.format('<div class="col-xs-12 col-sm-6 col-md-3" id="%s">', elementID) +
        '<div class="thumbnail">' +
        util.format('<img src="%s" alt="picture-placeholder">', img) +
        '<div class="caption">' +
        util.format('<h3 class="centered-text">%s</h3>', title) +
        util.format('<p class="centered-text" id="item-detail-text">%s</p>', detail) +
        '<p class="centered-text">' +
        util.format('<a class="btn btn-primary clickable" role="button" id="%s">Download</a>', downloadID) +
        '</p>' + '</div>' + '</div>' + '</div>';
    
    // insert the html after the specified period
    setTimeout(function() {
        // insert the html in to the page
        $('#newest-packages').append(html);
        loadScript('holder.js');
        
        // fade in the html
        $('#' + elementID).hide().fadeIn(300, function() {
            
            // subscribe the on-click event
            $('#' + downloadID).click(function(event) {
                bootbox.alert('Clicked ' + event.target.id);
            });
        });
    }, (count * 500));
}